Outdated browsers Primo UI toast message for the iDiscover service
==================================================================

Checks to see if you're running a recent version of popular browsers on the iDiscover service (see http://idiscover.lib.cam.ac.uk/) and advises you to upgrade if you're not.

Absolutely critical when we first when live with Primo New UI in September 2016. Since we migrated to Primo/Alma in January 2018 it's no longer important.

## Usage

Incorporated into the Primo customisation package used for iDiscover.
